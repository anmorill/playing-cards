package no.ntnu.idi.playingcards;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {
  DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  @DisplayName("A new deck should have 52 cards")
  void constructor() {
    assertEquals(52, deck.getCount());
  }

  @Test
  @DisplayName("Deal 5 cards")
  void dealHand() {
    var hand = deck.dealHand(5);
    assertEquals(5, hand.cards().size());
    assertEquals(47, deck.getCount());
  }

  @Test
  @DisplayName("Deal all cards")
  void dealHandAll() {
    var hand = deck.dealHand(52);
    assertEquals(52, hand.cards().size());
    assertEquals(0, deck.getCount());
  }

  @Test
  @DisplayName("Try to deal too many cards")
  void dealHandTooMany() {
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
  }

  @Test
  @DisplayName("Try to deal from an empty deck")
  void dealHandEmpty() {
    deck.dealHand(52);
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(1));
  }
}
