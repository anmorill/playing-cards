package no.ntnu.idi.playingcards;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HandTest {
  Hand hand;

  @BeforeEach
  void setUp() {
    List<Card> cards = new ArrayList<>();
    cards.add(new Card(Card.Suit.HEARTS, Card.Rank.ACE));
    cards.add(new Card(Card.Suit.SPADES, Card.Rank.QUEEN));
    cards.add(new Card(Card.Suit.HEARTS, Card.Rank.TWO));
    cards.add(new Card(Card.Suit.CLUBS, Card.Rank.NINE));
    cards.add(new Card(Card.Suit.DIAMONDS, Card.Rank.JACK));

    this.hand = new Hand(cards);
  }

  @Test
  void getSumOfFaces() {
    assertEquals(35, hand.getSumOfFaces());
  }

  @Test
  void getHeartCount() {
    assertEquals(2, hand.getHeartCount());
  }

  @Test
  void hasFlush() {
    assertFalse(hand.hasFlush());
  }

  @Test
  void hasQueenOfSpades() {
    assertTrue(hand.hasQueenOfSpades());
  }
}
