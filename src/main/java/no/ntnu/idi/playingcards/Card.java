package no.ntnu.idi.playingcards;

public record Card(Suit suit, Rank rank) {
  /**
   * The suit of a card
   */
  public enum Suit {
    SPADES, HEARTS, DIAMONDS, CLUBS
  }

  /**
   * The rank of a card
   */
  public enum Rank {
    ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
  }

  /**
   * Returns the suit of the card.
   */
  @Override
  public Suit suit() {
    return suit;
  }

  /**
   * Returns the rank of the card.
   */
  @Override
  public Rank rank() {
    return rank;
  }

  public char getSuitSymbol() {
    return switch (suit) {
      case SPADES -> '♠';
      case HEARTS -> '♥';
      case DIAMONDS -> '♦';
      case CLUBS -> '♣';
    };
  }

    public String getRankSymbol() {
        return switch (rank) {
        case ACE -> "A";
        case TWO -> "2";
        case THREE -> "3";
        case FOUR -> "4";
        case FIVE -> "5";
        case SIX -> "6";
        case SEVEN -> "7";
        case EIGHT -> "8";
        case NINE -> "9";
        case TEN -> "10";
        case JACK -> "J";
        case QUEEN -> "Q";
        case KING -> "K";
        };
    }

  /**
   * Construct a card using suit and rank
   *
   * @param suit The suit
   * @param rank The rank
   */
  public Card {
  }
}
