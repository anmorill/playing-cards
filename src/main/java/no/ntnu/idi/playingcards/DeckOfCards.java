package no.ntnu.idi.playingcards;

import java.util.ArrayList;
import java.util.List;

public class DeckOfCards {
  private final ArrayList<Card> cards;

  /**
   * Creates a new deck of 52 cards, with one card of each rank and suit.
   */
  public DeckOfCards() {
    cards = new ArrayList<>();

    for (Card.Suit suit : Card.Suit.values()) {
      for (Card.Rank rank : Card.Rank.values()) {
        cards.add(new Card(suit, rank));
      }
    }
  }

  /**
   * Returns n shuffled cards from the deck. The cards are removed from the deck.
   *
   * @param n The number of cards to deal
   * @return All cards in a  collection, where each card is a string of the format "H2"
   *        (for the 2 of hearts), "S11" (for the jack of spades), etc.
   * @throws IllegalArgumentException if n is greater than the number of cards in the deck
   */
  public Hand dealHand(int n) throws IllegalArgumentException {
    if (n > cards.size()) {
      throw new IllegalArgumentException("Not enough cards in the deck");
    }

    List<Card> cards = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      int index = (int) (Math.random() * this.cards.size());
      cards.add(this.cards.remove(index));
    }

    return new Hand(cards);
  }

  /**
   * Returns the number of cards left in the deck.
   */
  public int getCount() {
    return cards.size();
  }
}
