package no.ntnu.idi.playingcards;

import java.io.IOException;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class PlayingCardsApplication extends Application {
  private DeckOfCards deck;
  private Hand currentHand;

  public PlayingCardsApplication() {
    this.deck = new DeckOfCards();
  }

  @Override
  public void start(Stage stage) throws IOException {
    // Buttons
    Button btnDealHand = new Button("Deal hand");
    Button btnCheckHand = new Button("Check hand");

    // Labels and TextFields for the card hand information
    Label lblSumOfFaces = new Label("Sum of the faces:");
    TextField txtSumOfFaces = new TextField("25"); // Example value
    Label lblCardsOfHearts = new Label("Cards of hearts:");
    TextField txtCardsOfHearts = new TextField("H12 H9 H1"); // Example value
    Label lblFlush = new Label("Flush:");
    TextField txtFlush = new TextField("Yes/No"); // Example placeholder
    Label lblQueenOfSpades = new Label("Queen of spades:");
    TextField txtQueenOfSpades = new TextField("Yes/No"); // Example placeholder

    // Layout for the buttons
    HBox hboxButtons = new HBox(10, btnDealHand, btnCheckHand);

    // Layout for the hand information
    GridPane gridPaneInfo = new GridPane();
    gridPaneInfo.setVgap(10);
    gridPaneInfo.setHgap(10);
    gridPaneInfo.addRow(0, lblSumOfFaces, txtSumOfFaces);
    gridPaneInfo.addRow(1, lblCardsOfHearts, txtCardsOfHearts);
    gridPaneInfo.addRow(2, lblFlush, txtFlush);
    gridPaneInfo.addRow(3, lblQueenOfSpades, txtQueenOfSpades);

    // Main layout
    VBox vboxMain = new VBox(10);
    vboxMain.getChildren().addAll(hboxButtons, gridPaneInfo);

    VBox cardView = new VBox(10);

    // Placeholder for the card display area
    cardView.setPrefSize(300, 200); // Example size, adjust as needed

    // Adding everything to the main layout
    vboxMain.getChildren().addFirst(cardView); // Add at the top

    btnDealHand.setOnAction(e -> {
      cardView.getChildren().clear();

      if (deck.getCount() < 5) {
        deck = new DeckOfCards();
      }

      Hand hand = deck.dealHand(5);

      for (Card card : hand.cards()) {
        cardView.getChildren().add(createCard(card));
      }

      this.currentHand = hand;
    });

    btnCheckHand.setOnAction(e -> {
      txtSumOfFaces.setText(String.valueOf(currentHand.getSumOfFaces()));
      txtCardsOfHearts.setText(String.valueOf(currentHand.getHeartCount()));
      txtFlush.setText(currentHand.hasFlush() ? "Yes" : "No");
      txtQueenOfSpades.setText(currentHand.hasQueenOfSpades() ? "Yes" : "No");
    });

    Scene scene = new Scene(vboxMain, 500, 400);

    stage.setTitle("Frame");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch();
  }

  private StackPane createCard(Card card) {
    // Create a label with the card's value and suit
    Label lblCard = new Label(card.getRankSymbol() + card.getSuitSymbol());
    lblCard.setFont(new Font("Arial", 24)); // Set the font size

    // Create a StackPane to hold the label and give it a border
    StackPane stackPane = new StackPane();
    stackPane.setMaxWidth(50);
    stackPane.setStyle("-fx-border-color: black; -fx-border-width: 2px;");
    stackPane.setAlignment(Pos.CENTER);
    stackPane.getChildren().add(lblCard);

    return stackPane;
  }
}
