package no.ntnu.idi.playingcards;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class PlayingCardsController {
  @FXML
  private Label welcomeText;

  @FXML
  protected void onHelloButtonClick() {
    welcomeText.setText("Welcome to JavaFX Application!");
  }
}
