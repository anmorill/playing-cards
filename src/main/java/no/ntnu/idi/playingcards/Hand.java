package no.ntnu.idi.playingcards;

import java.util.List;

/**
 * A hand of cards.
 */
public record Hand(List<Card> cards) {
  /**
   * Returns the cards in the hand.
   */
  @Override
  public List<Card> cards() {
    return cards;
  }

  /**
   * Creates a new hand of cards.
   *
   * @param cards The cards to add to the hand.
   */
  public Hand {
  }

  /**
   * Returns the sum of the faces of the cards in the hand.
   */
  public int getSumOfFaces() {
    return cards.stream().mapToInt(card -> card.rank().ordinal() + 1).sum();
  }

  /**
   * Returns the number of hearts in the hand.
   */
  public int getHeartCount() {
    return (int) cards.stream().filter(card -> card.suit() == Card.Suit.HEARTS).count();
  }

  /**
   * Returns whether the hand has a flush.
   */
  public boolean hasFlush() {
    return cards.stream().map(Card::suit).distinct().count() == 1;
  }

  /**
   * Returns whether the hand has the queen of spades.
   */
  public boolean hasQueenOfSpades() {
    return cards.stream()
        .anyMatch(card -> card.suit() == Card.Suit.SPADES && card.rank() == Card.Rank.QUEEN);
  }
}
